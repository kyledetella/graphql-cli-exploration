# graphql-cli exploration

Exploring the [graphql-cli](https://github.com/graphql-cli/graphql-cli)


**Generate types**

`npx graphql codegen`

**Visualize w/Voyager**

`npx graphql voyager`