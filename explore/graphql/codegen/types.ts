/* tslint:disable */
//  This file was automatically generated and should not be edited.

export interface GetTodosQuery {
  // Fetch all available Todos
  todos:  Array< {
    // Unique ID of a Todo
    id: string,
    // The description fo the Todo
    description: string,
  } | null >,
};

export interface GetTodoQueryVariables {
  id: string,
};

export interface GetTodoQuery {
  // Fetch a single Todo
  todo:  {
    // The display title for a Todo
    title: string,
    // The description fo the Todo
    description: string,
    // Whether the Todo is complete
    complete: boolean,
  } | null,
};

export interface GTQuery {
  // Fetch all available Todos
  todos:  Array< {
    // Unique ID of a Todo
    id: string,
  } | null >,
};
