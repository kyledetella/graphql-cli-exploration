export const getTodos = gql`
  query GetTodos {
    todos {
      id
      description
    }
  }
`;

export const getTodo = gql`
  query GetTodo($id: ID!) {
    todo(id: $id) {
      title
      description
      complete
    }
  }
`;
